class Note {
  String id;
  String title;
  String note;

  Note({this.id = '', required this.title, required this.note});
}
