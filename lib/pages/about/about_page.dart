import 'package:flutter/material.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ListView(
        children: [
          Text(
            "About",
            style: Theme.of(context).textTheme.headline2,
          ),
          const SizedBox(height: 16.0),
          Text(
            'This app was created for the learners of Ground Gurus! 🥳🎉🎊',
            style: Theme.of(context).textTheme.bodyText2,
          ),
          const SizedBox(height: 16.0),
          Image.network(
            'https://groundgurus-assets.s3.ap-southeast-1.amazonaws.com/Ground+Gurus+-+Logo+Small.png',
          ),
        ],
      ),
    );
  }
}
