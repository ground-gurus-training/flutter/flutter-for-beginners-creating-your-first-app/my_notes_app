import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_notes_app/controllers/notes_controller.dart';

import '../../model/note.dart';
import '../notes/note_edit.dart';

class HomeNote extends StatelessWidget {
  final NotesController controller;
  final Note note;

  const HomeNote({super.key, required this.controller, required this.note});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5.0,
      child: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  note.title,
                  style: Theme.of(context).textTheme.headline2,
                ),
                Expanded(child: Container()),
                PopupMenuButton(
                  itemBuilder: (builder) => [
                    PopupMenuItem(
                      child: TextButton(
                        onPressed: () {
                          Get.back();
                          Get.to(() => NoteEdit(
                                controller: controller,
                                note: note,
                              ));
                        },
                        child: const Text('Edit'),
                      ),
                    ),
                    PopupMenuItem(
                      child: TextButton(
                        onPressed: () {
                          Get.back();
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Text(
                                    'Delete ${note.title}',
                                    maxLines: 1,
                                    softWrap: false,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  content: const Text(
                                      'Are you sure you want to delete this note?'),
                                  actions: [
                                    ElevatedButton(
                                      onPressed: () {
                                        controller.deleteNote(note);
                                        Get.back();
                                      },
                                      child: const Text('Yes'),
                                    ),
                                    ElevatedButton(
                                      onPressed: () => Get.back(),
                                      child: const Text('No'),
                                    ),
                                  ],
                                );
                              });
                        },
                        child: const Text('Delete'),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 10.0),
            Row(
              children: [
                Text(
                  note.note,
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
