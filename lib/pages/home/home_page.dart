import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_notes_app/controllers/notes_controller.dart';
import 'package:my_notes_app/pages/home/home_note.dart';

class HomePage extends StatelessWidget {
  final NotesController controller;

  const HomePage({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Padding(
        padding: const EdgeInsets.all(16.0),
        child: ListView.builder(
          itemCount: controller.notes.length,
          itemBuilder: (BuildContext context, int index) {
            return HomeNote(
              controller: controller,
              note: controller.notes[index],
            );
          },
        ),
      ),
    );
  }
}
