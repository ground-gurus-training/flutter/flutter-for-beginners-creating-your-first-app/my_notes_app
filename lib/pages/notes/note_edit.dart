import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_notes_app/controllers/notes_controller.dart';

import '../../model/note.dart';

class NoteEdit extends StatelessWidget {
  final Note note;
  final NotesController controller;

  const NoteEdit({super.key, required this.controller, required this.note});

  @override
  Widget build(BuildContext context) {
    TextEditingController titleController = TextEditingController();
    titleController.text = note.title;

    TextEditingController noteController = TextEditingController();
    noteController.text = note.note;

    return Scaffold(
      body: Form(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              TextFormField(
                controller: titleController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Title',
                ),
              ),
              const SizedBox(height: 16.0),
              TextFormField(
                controller: noteController,
                maxLines: 5,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Note',
                ),
              ),
              const SizedBox(height: 16.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      note.title = titleController.text;
                      note.note = noteController.text;
                      controller.updateNote(note);
                      Get.back();
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          const Icon(Icons.save),
                          const SizedBox(width: 4.0),
                          Text(
                            'Save',
                            style: Theme.of(context).textTheme.button,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
