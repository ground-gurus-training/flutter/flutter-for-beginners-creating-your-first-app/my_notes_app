import 'package:flutter/material.dart';

final myTheme = ThemeData(
  appBarTheme: AppBarTheme(
    backgroundColor: Colors.yellow[700],
    titleTextStyle: const TextStyle(
      color: Colors.white,
      fontSize: 20.0,
    ),
  ),
  textTheme: const TextTheme(
    headline2: TextStyle(
      fontSize: 28.0,
      color: Colors.black,
    ),
    bodyText2: TextStyle(
      fontSize: 16.0,
    ),
    button: TextStyle(
      fontSize: 20.0,
      color: Colors.white,
    ),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      padding: MaterialStateProperty.resolveWith((states) => EdgeInsets.zero),
      backgroundColor: MaterialStateProperty.resolveWith(
        (states) => Colors.yellow[700],
      ),
    ),
  ),
  floatingActionButtonTheme: FloatingActionButtonThemeData(
    backgroundColor: Colors.yellow[700],
  ),
  navigationBarTheme: NavigationBarThemeData(
    indicatorColor: Colors.yellow[700],
  ),
);
