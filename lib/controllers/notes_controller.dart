import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';

import '../model/note.dart';

class NotesController extends GetxController {
  List<Note> notes = [
    Note(title: '', note: ''),
  ].obs;

  loadNotes(docs) {
    notes.removeWhere((element) => true);
    for (var doc in docs) {
      notes.add(Note(id: doc.id, title: doc['title'], note: doc['note']));
    }
  }

  addNote(Note note) {
    final newNote = <String, dynamic>{
      'title': note.title,
      'note': note.note,
    };

    var db = FirebaseFirestore.instance;
    db.collection('Notes').add(newNote).then((DocumentReference doc) {
      note.id = doc.id;
      notes.add(note);
    });
  }

  deleteNote(note) {
    var db = FirebaseFirestore.instance;
    db.collection('Notes').doc(note.id).delete().then((doc) {
      notes.remove(note);
    });
  }

  sort() {
    notes.sort((a, b) => a.title.compareTo(b.title));
  }

  updateNote(Note note) {
    var searchedNote =
        notes.firstWhereOrNull((element) => element.id == note.id);
    if (searchedNote != null) {
      deleteNote(searchedNote);
      addNote(note);
    } else {
      addNote(note);
    }
    sort();
  }
}
