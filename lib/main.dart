import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_notes_app/pages/about/about_page.dart';
import 'package:my_notes_app/pages/home/home_page.dart';
import 'package:my_notes_app/themes/my_theme.dart';

import 'controllers/notes_controller.dart';
import 'firebase_options.dart';
import 'model/note.dart';
import 'pages/notes/note_edit.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final Future<FirebaseApp> _fbApp = Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  final controller = Get.put(NotesController());
  int currentIndex = 0;
  late List<Widget> pages;
  bool notesLoaded = false;

  @override
  void initState() {
    super.initState();
    pages = <Widget>[
      HomePage(controller: controller),
      const AboutPage(),
    ];
  }

  Future<void> loadNotes() async {
    if (notesLoaded) return;

    var db = FirebaseFirestore.instance;
    final notes = db.collection('Notes');
    final snapshot = await notes.get();
    controller.loadNotes(snapshot.docs);
    notesLoaded = true;
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: myTheme,
      debugShowCheckedModeBanner: false,
      home: FutureBuilder(
        future: _fbApp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            debugPrint('You have an error! ${snapshot.error.toString()}');
            return const Scaffold(
              body: Center(
                child: Text('Something went wrong!'),
              ),
            );
          } else if (snapshot.hasData) {
            loadNotes();
            return Scaffold(
              appBar: AppBar(
                title: const Text('My Notes'),
              ),
              body: pages[currentIndex],
              floatingActionButton: FloatingActionButton(
                onPressed: () => Get.to(
                  () => NoteEdit(
                    controller: controller,
                    note: Note(title: '', note: ''),
                  ),
                ),
                child: const Icon(Icons.add),
              ),
              bottomNavigationBar: NavigationBar(
                selectedIndex: currentIndex,
                onDestinationSelected: (index) => setState(() {
                  currentIndex = index;
                }),
                destinations: const [
                  NavigationDestination(
                    icon: Icon(Icons.house),
                    label: 'Home',
                  ),
                  NavigationDestination(
                    icon: Icon(Icons.person),
                    label: 'About',
                  ),
                ],
              ),
            );
          } else {
            return const Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        },
      ),
    );
  }
}
